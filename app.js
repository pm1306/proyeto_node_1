//  Requireds
// const fs = require('express');  LKibreria que instalar
// const fs = require('./../fs');  Definidos por uno.
/*
const fs = require('fs');
let base = 5;
let data = '';
 
for(let i = 1; i<=10; i++){
  data += `${ base } * ${ i } =  ${ base*i} \n`;
}

fs.writeFile(`tablas/tabla-${ base }.txt`, data, (err) => {
  if(err) throw err;
  console.log(`archivo tabla-${ base }; fue creado con exito`);
});

*/
//const multiplicar = require('./tablas/multiplicar');
const argv = require('yargs')
      .command('listar', 'Lista de comandos de ayuda', 
      {
          base: { 
            demand: true,
             alias: 'b'
          },
          limite: { 
            default: 10,
            alias: 'l'
          }
      })
      .help()
      .argv;
const { crearArchivo } = require('./tablas/multiplicar');
let comando = argv._[0];

switch(comando){
  case 'listar': console.log('listar'); break;
  case 'crear': console.log(`crear  tabla para : ${ argv.base }`); 
  crearArchivo(argv.base, argv.limite)
  .then( archivo => console.log(`Archivo creado: ${ archivo }`))
  .catch(e => console.log(e));
  break; 
  default :  console.log('Comando no reconocido,  utilice  listar --help');
}



//console.log(module);  //-> ver todos las direcciones que inicia los archivos
//console.log(multiplicar); 
// let base = 5;  Base inicializada desde e programa
// Para base pasada por parametros de la linea de comandos.
//console.log(argv);
//console.log(argv.limite);
//console.log(argv.base);
//let parametro = process.argv[2];
//let base = parametro.split('=')[1];

/*
let argv2 = process.argv;
console.log(argv);
console.log('*********************+');
console.log(argv2);
*/
//console.log(`base es : ${ base }`);



/*
crearArchivo(base)
  .then( archivo => console.log(`Archivo creado: ${ archivo }`))
  .catch(e => console.log(e));
*/
  //console.log(process); =  Procesos de node cuando se crea una variable